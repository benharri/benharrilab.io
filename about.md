---
layout: page
title: About
permalink: /about/
---

I studied computer science at Northern Michigan University. I'm into full-stack web development. I love to travel and have lived in Ecuador and Switzerland.

One of my pet projects is [tilde.team](https://tilde.team/), a clone of [tilde.club](http://tilde.club) that I created when I was unable to get an account there. My user profile there is [~ben](https://tilde.team/~ben/).

These are some of my other profiles.

* [gitlab](https://gitlab.com/benharri)
* [github](https://github.com/benharri)
* [telegram](https://t.me/bharris)
* [bsky](https://bsky.app/profile/benharr.is)
* [linkedin](https://www.linkedin.com/in/benjaminhharris)
* [insta](https://instagram.com/benharr.is)
* [keybase](https://keybase.io/bharris)
* [email](mailto:ben@tilde.team)

You can also find me at my [personal site](https://benharr.is/) or my [indieweb hub](https://benharri.org/).
